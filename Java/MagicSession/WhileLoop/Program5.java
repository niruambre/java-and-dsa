//WAP to print even digit square of given number
class Square{
	public static void main(String args[]){
		int num=123498;
		while(num!=0){
			int dig=num%10;
			num=num/10;
			if(dig%2==0){
				System.out.println(dig*dig);
			}
		}
	}
}
	

