//WAP to count the digit of even Number
class Even{
	public static void main(String args[]){
		int num=123986;
		int count=0;
		while(num!=0){
			if(num%2==0){
				int digit=num%10;
				num=num/10;
				count++;
			}
		}
		System.out.println("Count of even Number is:"+count);
	}
}
