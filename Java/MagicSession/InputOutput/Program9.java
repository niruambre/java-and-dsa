//WAP to take a number as input and print the addition of factorials each digit from that number
import java.io.*;
class Question9{
	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Number:");
		int num=Integer.parseInt(br.readLine());
		int sum=0;
		for(int i=num;i!=0;i=i/10){
			int rem=i%10;
			int fact=1;
			for(int j=1;j<=rem;j++){
				fact=fact*j;
			}
			sum=sum+fact;
		}
		System.out.println("Addition of factorials of each digit of "+num+" "+ "is"+" "+sum);	
	}
}
