/* WAP and take two character if these characters are equal then print them as it is but if they are unequal then print their difference
 */
import java.io.*;
class Question6{
	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter First Character:");
		char ch1=(char)br.read();
		br.skip(1);
		System.out.println("Enter Second Character:");                                                                                char ch2=(char)br.read();  
		if(ch1==ch2){
			System.out.println(ch1+" "+"and"+" "+ch2+" "+"are equal");
		}else{
			int dif=ch2-ch1;
			System.out.println("Difference between"+" "+ch1+" "+"and"+" "+ch2+" "+"is:"+dif);
		}
	}
}
