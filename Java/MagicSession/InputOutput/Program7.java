/*
 O
 14 13
 L  K  J
 9  8  7  6
 E  D  C  B  A
 */
import java.io.*;
class Question7{
	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Number of Rows:");
		int row=Integer.parseInt(br.readLine());
		int num=row*(row+1)/2;
		char ch=(char)(64+num);
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				if(i%2==1){
					System.out.print(ch+""+"    ");
				}else{
					System.out.print(num+"    ");
				}
				ch--;
				num--;
			}
		System.out.println();
		}
	}
}
