/*
 5 4 3 2 1
 8 6 4 2
 9 6 3
 8 4
 5
 */
import java.io.*;
class Question3{
	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Number of Rows");
		int row=Integer.parseInt(br.readLine());
		int num=row;
		for(int i=1;i<=row;i++){
			for (int j=row;j>=i;j--){
				System.out.print(num*i+" ");
				num--;
			}
			num=row-i;
			System.out.println();
		}
	}
}

