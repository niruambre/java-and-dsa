/* 
 0
 1 1
 2 3 5 
 8 13 21 24
 */
import java.io.*;
class Question4{
	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int a=1;
		int b=1;
		System.out.println("Enter Number of Rows");
		int row=Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				a=b-a;
				b=a+b;
				System.out.print(a+" ");
			}
			System.out.println();
		}
	}
}
