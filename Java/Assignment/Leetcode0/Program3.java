// Search insert position
class Question3{
    static int searchInsert(int arr[], int n) {
        int left = 0;
        int right = arr.length - 1;
        
        while (left <= right) {
            int mid = left + (right - left) / 2; 
            
            if (arr[mid] == n) {
                return mid;
            } else if (arr[mid] < n) {
                left = mid + 1; 
            } else {
                right = mid - 1; 
            }
        }
        
        return left;
    }

    public static void main(String[] args) {
        int arr[] = new int[]{1, 3, 5, 6};
        int tar = 5;
        int ans =searchInsert(arr, tar);
        System.out.println(ans); 
    }
}

