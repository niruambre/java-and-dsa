/*
 * Program - 9
 * WAP to print the second max element in the array.
 */

import java.util.*;
class Question9{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array elements : ");
		for(int i=0; i<arr.length;i++){
			arr[i] = sc.nextInt();
		}

		System.out.print("Second Max element in the array is : ");
		int max = arr[0];
		for(int i=0; i<arr.length; i++){
			if(arr[i]>max){
				max = arr[i];
			}
		}
		int secMax = arr[0];
		for(int i=0; i<arr.length; i++){
			if(arr[i]>secMax && arr[i] != max){
				secMax = arr[i];
			}
		}
		System.out.print(secMax);
		System.out.println();
	}
}
