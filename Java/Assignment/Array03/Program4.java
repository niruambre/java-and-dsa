/*
 * Program - 4 
 * WAP to find a prime number from an array and return its index.
 * Take size and elements from the user.
 */

import java.util.*;
class Question4{
	void primeNo(int num, int index){
		int count = 0;
		for(int i=1; i<=num;i++){
			if(num%i == 0){
				count++;
			}
			if(count == 3){
				break;
			}
		}
		if(count == 2){
			System.out.println("Prime number " + num + " found at index : " + index);
		}
	}

	public static void main(String[] args){
		Question4 obj = new Question4();

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array elements : ");
		for(int i=0; i<arr.length;i++){
			arr[i] = sc.nextInt();
		}
	
		for(int i=0; i<arr.length; i++){
			obj.primeNo(arr[i], i);
		}
	}
}
