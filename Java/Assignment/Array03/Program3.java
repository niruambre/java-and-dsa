/*
 * Program - 3 
 * WAP to find a composite number from an array and return its index.
 * Take size and elements from the user.
 */

import java.util.*;
class Question3{
	void compositeNo(int num, int index){
		int count = 0;
		for(int i=1; i<=num;i++){
			if(num%i == 0){
				count++;
			}
			if(count == 3){
				break;
			}
		}
		if(count == 3){
			System.out.println("Composite number " + num + " found at index : " + index);
		}
	}

	public static void main(String[] args){
		Question3 obj = new Question3();

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array elements : ");
		for(int i=0; i<arr.length;i++){
			arr[i] = sc.nextInt();
		}

		for(int i=0; i<arr.length; i++){
			obj.compositeNo(arr[i], i);
		}
	}
}
