/*
 * Program - 2 
 * WAP to reverse each element in an array.
 * Take size and element from the user.
 */

import java.util.*;
class Question2{
	void revElement(int num){
		int rev = 0;
		for(int i=num; i!=0; i=i/10){
			rev = (rev * 10) + (i%10);
		}
		System.out.print(rev + " ");
	}

	public static void main(String[] args){
		Question2 obj = new Question2();

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array elements : ");
		for(int i=0; i<arr.length;i++){
			arr[i] = sc.nextInt();
		}

		System.out.print("Reverse of each element in an array is : ");
		for(int i=0; i<arr.length; i++){
			obj.revElement(arr[i]);
		}
		System.out.println();
	}
}
