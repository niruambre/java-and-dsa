/*
 * Program - 5 
 * WAP to find a perfect number from an array and return its index.
 * Take size and elements from the user.
 */

import java.util.*;
class Question5{
	void perfectNo(int num, int index){
		int sum = 0 ;
		for(int i=1; i<=num; i++){
			if(num%i == 0){
				if(i != num){
					sum = sum + i;
				}
			}
		}
		if(sum == num){
			System.out.println("Perfect number " + num + " found at index : " + index);
		}
	}
	public static void main(String[] args){
		Question5 obj = new Question5();

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array elements : ");
		for(int i=0; i<arr.length;i++){
			arr[i] = sc.nextInt();
		}
		
		for(int i=0; i<arr.length; i++){
			obj.perfectNo(arr[i], i);
		}
	}
}
