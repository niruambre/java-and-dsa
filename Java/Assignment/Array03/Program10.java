/*
 * Program - 10
 * WAP to print the second min element in the array.
 */

import java.util.*;
class Question10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array elements : ");
		for(int i=0; i<arr.length;i++){
			arr[i] = sc.nextInt();
		}

		System.out.print("Second Min element in the array is : ");
		int min = arr[0];
		for(int i=0; i<arr.length; i++){
			if(arr[i]<min){
				min = arr[i];
			}
		}
		int secMin = arr[0];
		for(int i=0; i<arr.length; i++){
			if(arr[i]<secMin && arr[i] != min){
				secMin = arr[i];
			}
		}
		System.out.print(secMin);
		System.out.println();
	}
}
