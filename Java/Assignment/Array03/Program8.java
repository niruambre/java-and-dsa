/*
 * Program - 8
 * WAP to find a armstrong number from an array and return its index.
 * Take size and elements from the user.
 */

import java.util.*;
class Question8{
	void armstrongNo(int num, int index){
		int sum = 0;
		int count = 0;
		for(int i=num; i!=0; i=i/10){
			count++;
		}
		for(int i=num; i!=0; i=i/10){
			int product = 1;
			for(int j=1; j<=count; j++){
				product = product * (i%10);
			}
			sum = sum + product;
		}
		if(num == sum){
			System.out.println("Armstrong number " + num + " found at index : " + index);
		}
	}
	public static void main(String[] args){
		Question8 obj = new Question8();

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array elements : ");
		for(int i=0; i<arr.length;i++){
			arr[i] = sc.nextInt();
		}

		for(int i=0; i<arr.length; i++){
			obj.armstrongNo(arr[i], i);
		}
	}
}
