/*
 * Program - 6
 * WAP to find a palindrome number from an array and return its index.
 * Take size and elements from the user.
 */

import java.util.*;
class Question6{
	void palindromeNo(int num, int index){
		int rev = 0;
		for(int i=num; i!=0; i=i/10){
			rev = (rev*10) + (i%10);
		}
		if(num == rev){
			System.out.println("Palindrome number " + num + " found at index : " + index);
		}
	}
	public static void main(String[] args){
		Question6 obj = new Question6();

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array elements : ");
		for(int i=0; i<arr.length;i++){
			arr[i] = sc.nextInt();
		}

		for(int i=0; i<arr.length; i++){
			obj.palindromeNo(arr[i], i);
		}
	}
}
