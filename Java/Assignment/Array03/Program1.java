/*
 * Program - 1
 * WAP to print count of digits in elements of array
 */

import java.util.*;
class Question1{
	void cntDigits(int num){
		int count = 0;
		for(int i=num; i!=0; i=i/10){
			count++;
		}
		System.out.print(count + " ");
	}
	public static void main(String[] args){
		Question1 obj = new Question1();

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array elements : ");
		for(int i=0; i<arr.length;i++){
			arr[i] = sc.nextInt();
		}

		System.out.print("Count of digits in elements of array is : ");
		for(int i=0; i<arr.length; i++){
			obj.cntDigits(arr[i]);
		}
		System.out.println();
	}
}
