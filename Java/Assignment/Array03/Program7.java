/*
 * Program - 7
 * WAP to find a strong number from an array and return its index.
 * Take size and elements from the user.
 */

import java.util.*;
class Question7{
	void strongNo(int num, int index){
		int sum = 0;
		for(int i=num; i!=0; i=i/10){
			int fact = 1;
			for(int j=1; j<=(i%10); j++){
				fact = fact * j;
			}
			sum = sum + fact;
		}
		if(num == sum){
			System.out.println("Strong number " + num + " found at index : " + index);
		}
	}
	public static void main(String[] args){
		Question7 obj = new Question7();

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array elements : ");
		for(int i=0; i<arr.length;i++){
			arr[i] = sc.nextInt();
		}

		for(int i=0; i<arr.length; i++){
			obj.strongNo(arr[i], i);
		}
	}
}
