//Find Max and Min Element of an Array and print addition of min max elemrnt also display min and max element
class Question1{
	public static void main(String args[]){
		int n=5;
		int arr[]=new int[]{-2,1,-4,5,3};
		int min=Integer.MAX_VALUE;
		int max=Integer.MIN_VALUE;
		int sum=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]>max){
				max=arr[i];
			}
			if(arr[i]<min){
				min=arr[i];
			}
		}
		sum=max+min;
		System.out.println("Maximum Element is "+ max + " and " + "Minimum Element is "+ min + " "+ (max "+" min)+ " =" + sum);
	}
}
