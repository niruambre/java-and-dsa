//Linear Search Multiple Occurences Given an array A and int B find the number of Occurences of B in A
class Question2{

	static void occurence(int A[],int B){
		int count=0;
		for(int i=0;i<A.length;i++){
			if(A[i]==B){
				count++;
			}
		}
		if(count>=1){
			System.out.println("Count of Element Equal to " + B + " is " + count);
		}else{
			System.out.println("There is No such element Equal to " + B);
		}
	}
	public static void main(String args[]){
               int A[]=new int []{1,2,2};
               int B=2;
               occurence(A,B);
	}
}
