//WAP to find sum of even and odd number in an array
import java.util.*;
class Question3{
	public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enetr array Size:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		int evenSum=0;
		int oddSum=0;
		System.out.println("Enter array Element:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
			if(arr[i]%2==0){
				evenSum=evenSum+arr[i];
			}else{
				oddSum=oddSum+arr[i];
			}
		}
		System.out.println("Even Number sum is: "+evenSum);
		System.out.println("Odd Number sum is: "+oddSum);
	}
}
