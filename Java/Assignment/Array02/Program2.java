//WAP to find even and odd element in an array
import java.util.*;
class Question2{
	public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Array Size:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		int countEven=0;
		int countOdd=0;
		System.out.println("Enter Array Element:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
			if(arr[i]%2==0){
				countEven++;
			}else{
				countOdd++;
			}
		}
		System.out.println("Number of Even Element is: "+countEven);
		System.out.println("Number of odd Element is: "+countOdd);
	}
}

