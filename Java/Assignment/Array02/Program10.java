//WAP to print the element whose addition of digit is even
import java.util.*;
class Question10{
	public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Array Size");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter Array Element:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Element whose addition of digit is even:");
		for(int i=0;i<arr.length;i++){
			int add=0;
			for(int j=arr[i];j!=0;j=j/10){
				add=add+(j%10);
			}
			if(add%2==0){
				System.out.print(arr[i] +" ");
			}
		}
		System.out.println();
	}
}



