//WAP to take an array from user with its size and find the minimum element from an array
import java.util.*;
class Question5{
	public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Size of an Array:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter Element of An Array:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int min=arr[0];
		for(int i=0;i<arr.length;i++){
			if(arr[i]<min){
				min=arr[i];
			}
		}
		System.out.println("Min Element is: "+min);
	}
}



