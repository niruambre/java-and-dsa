//WAP to take input from user no of element and array size and and find sum of all elements in the array
import java.util.*;
class Question1{
	public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter array size:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		int sum=0;
		System.out.println("Enter Array Element:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
			sum=sum+arr[i];
		}
		System.out.println("Sum of Array Element is: "+sum);
	}
}			
