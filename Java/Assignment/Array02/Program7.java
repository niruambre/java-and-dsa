//WAP to find common element between two array
import java.util.*;
class Question7{
	public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Size of an Array:");
		int size=sc.nextInt();
		int arr1[]=new int[size];
		int arr2[]=new int[size];
		System.out.println("Enter First Array:");
		for(int i=0;i<arr1.length;i++){
			arr1[i]=sc.nextInt();
		}
		System.out.println("Enter Second Array:");
		for(int i=0;i<arr2.length;i++){
			arr2[i]=sc.nextInt();
		}
		System.out.println("Common element are:");
		for(int i=0;i<arr1.length;i++){
			for(int j=0;j<arr2.length;j++){
				if(arr1[i]==arr2[j]){
					System.out.println(arr1[i]);
				}
			}
		}
	}
}
