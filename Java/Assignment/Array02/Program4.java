//WAP to search a specific element from an array and return its index 
import java.util.*;
class Question4{
	public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Array size:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter Search Element:");
		int search=sc.nextInt();
		System.out.println("Enter Array Element:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		for(int i=0;i<arr.length;i++){
			if(arr[i]==search){
				System.out.println("Element found at index: "+i);
			}
		}
	}
}

