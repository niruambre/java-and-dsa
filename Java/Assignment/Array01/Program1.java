//WAP to take size and element from user and print sum of all entered Element
import java.util.*;
class Question1{
	public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Array Size:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		int sum=0;
		System.out.println("Enter Array Element:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
			if(arr[i]%2==0){
				sum=sum+arr[i];
			}
		}
		System.out.println("Sum of even element in an array is: "+sum);
	}
}

