//WAP to take 7 characters as an input print only Vowels from the array
import java.util.*;
class Question4{
	public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Size of an array:");
		int size=sc.nextInt();
		char arr[]=new char[size];
		System.out.println("Enter character:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.next().charAt(0);
		}		
		System.out.println("Vowels in an array are:");
		for(int i=0;i<arr.length;i++){
			if(arr[i]=='a'||arr[i]=='A'||arr[i]=='e'||arr[i]=='E'||arr[i]=='i'||arr[i]=='I'||arr[i]=='o'||arr[i]=='O'||arr[i]=='u'||arr[i]=='U'){
				System.out.println(arr[i]);
			}
		}
	}
}
