// WAP to take array sie and input from user and print product of all even numer in an array
import java.util.*;
class Question2{
	public static void main(String args[]){
		int prod=1;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Size of an Array:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter Elements of an Array:"); 
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
			if(arr[i]%2==0){
				prod=prod*arr[i];
			}
		}
		System.out.println("Product of even Element in an array is: "+prod);
	}
}	
