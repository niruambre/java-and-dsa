//WAP to take input of size and array Element from user and print product of odd index only
import java.util.*;
class Question3{
	public static void main(String args[]){
		int prod=1;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Size of an Array:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter Element of an Array:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
			if(i%2!=0){
				prod=prod*arr[i];
			}
		}
		System.out.println("Product of odd index in an array is: "+prod);
	}
}	
